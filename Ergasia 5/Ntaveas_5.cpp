#include <CGAL/Cartesian_d.h>
#include <CGAL/point_generators_d.h>
#include <CGAL/Kd_tree.h>
#include <CGAL/Search_traits_d.h>
#include <CGAL/Orthogonal_k_neighbor_search.h>

#include <fstream>
#include <iostream>

#define PI 3.1415926
#define EUCLIDEAN 0
#define COSINE 1

typedef CGAL::Cartesian_d<double> K;
typedef K::Point_d Point_d;
typedef CGAL::Search_traits_d<K> Traits;
typedef CGAL::Random_points_in_cube_d<Point_d>       Random_points_iterator;
typedef CGAL::Counting_iterator<Random_points_iterator> N_Random_points_iterator;
typedef CGAL::Kd_tree<Traits> Tree;

typedef CGAL::Orthogonal_k_neighbor_search<Traits> Neighbor_search;
typedef Neighbor_search::Tree Tree;


struct Distance{
     
     Point_d A;
     Point_d B;
     
     double transformed_distance(const Point_d A, const Point_d B) const {
          
          //Formula of cos.distance
          //cos(angle) = S(A[i]*B[i]) / ( S(A[i]^2)^(1/2) * S(B[i]^2)^(1/2) )
          
          int i = 0;
          double cosine, dist;
          double sum_of_vertices = 0.0; 
          double norm = 0, norm_1 = 0, norm_2 = 0;
          double angle = 0;
          
          for(i = 0 ; i < A.dimension() ; i++ ){
               sum_of_vertices += A[i] * B[i];
               norm_1 += A[i] * A[i];
               norm_2 += B[i] * B[i];
          }

          norm = CGAL::sqrt(norm_1) * CGAL::sqrt(norm_2);
          cosine = sum_of_vertices / norm;
          //costan(cos(angle)) = angle
          angle = std::acos(cosine);
          dist = 1 - angle/PI;

          return dist;
     }
     
     double inverse_of_transformed_distance(double d) { return std::sqrt(d); }
};


int main( int argc , char* argv[]) {
     
     if (argc == 1){
          std::cout << "Error::Expected filename" << std::endl;
          return 0;
     }
     
     //input stream
     std::ifstream ifs (argv[1], std::ifstream::in);
     //output stream
     std::ofstream ofs;
     ofs.open ("results.txt");
     
     //start from fresh file
     ofs < "";
     
     //select type of distance
     //default metric is eucledean
     int type = EUCLIDEAN;
     
     if ( argc == 3 && strcmp(argv[2], "-cosine" ) == 0 )
          type = COSINE;
     
     //show the Metric
     std::cout << "Metric:";
     ofs  << "Metric:";
     
     if (type == 0){
          std::cout << "Eucledean" << std::endl;
          ofs  << "Eucledean" << std::endl;
     }
     else{
          std::cout << "Cosine" << std::endl;
          ofs  << "Cosine" << std::endl;
     }

     char c = ifs.get();
     char Dimensions[2]; int i = 0; //dimensions is 2digit number
     
     //Read Dimension from file
     while( c != '\n' ){
          Dimensions[i++] = c;
          c = ifs.get();
     }
     const int D = atoi(Dimensions);

     std::cout<< "Dimensions : " << D <<std::endl;
     ofs << "Dimensions : " << D <<std::endl;
     
     
     char buffer[32] ; i = 0;
     double vertices[D] ; int v = 0;
     
     //initialize buffers
     for( i = 0 ; i < D ; i++){
          vertices[i] = 0.0;
     }
     for( i = 0 ; i < 32 ; i++){
          buffer[i] = '\0';
     }
     
     i = 0;
     
     std::list<Point_d> points;
     
     c = ifs.get();
     
     //Read Points
     while (ifs.good()) {
          v = 0; 
          while( c != '\n' && c != EOF ){
               i = 0;
               while( c != ' ' && c != '\n' && c != EOF){
                    
                    buffer[i++] = c;
                    c = ifs.get();
               }

               vertices[v++] = atof(buffer);
               while( i >= 0 )
                    buffer[i--]='\0';
               
               if( c == '\n' )
                    break;
               c = ifs.get();
          }
          points.push_back(Point_d(D, vertices , vertices+D));
          while(v>=0)
               vertices[v--] = 0.0;
          c = ifs.get();
     }
     
     //close input file
     ifs.close();
     
     
     //number of wanted neighbors (default 1)
     const int N = 1;

     //Insert points in the tree
     Tree tree(points.begin(), points.end());

     //user defined distance
     Distance tr_dist;
     
     //start research
     std::cout << "Give Query: (Press 'q' to exit)" << std::endl;
     ofs  << "Give Query: (Press 'q' to exit)" << std::endl;
     
     int queries = 1; char order[32];
     
     while( 1 ){
          
          std::cout << std::endl << "Query " << queries << std::endl;
          ofs  << std::endl << "Query " << queries << std::endl;
          
          queries++;
          
          double  q_point[D];
          std::cout << "Query Point: " ;
          ofs  << "Query Point: " ;
          
          for( i = 0 ; i < D ; i++ ){
               std::cin >> order;
               
               if(order[0] == 'q'){
                    exit(1);
                    ofs.close();
               }
               q_point[i] = atof(order);
               ofs << q_point[i] << " ";
               
          }
          
          std::cout << std::endl;
          ofs  << std::endl;
          
          Point_d query(D, q_point, q_point+D);
               
          //search tree for N near neighbor
          Neighbor_search search(tree, query, N);
          std::cout << "Nearest neighbor: " ;
          ofs  << "Nearest neighbor: " ;
           
          for(Neighbor_search::iterator it = search.begin(); it != search.end(); ++it){
               
               if( type == EUCLIDEAN ){  
                    std::cout << it->first << " (at distance: " << std::sqrt(it->second) << ")"<< std::endl;
                    ofs  << it->first << " (at distance: " << std::sqrt(it->second) << ")"<< std::endl;
               }
               if( type == COSINE )
                    std::cout << it->first << " (at distance: " << tr_dist.transformed_distance(it->first , query) << ")"<< std::endl;   
                    ofs  << it->first << " (at distance: " << tr_dist.transformed_distance(it->first , query) << ")"<< std::endl;          
          }
             
     }
     
     std::cout << "Results are also written in 'results.txt'" << std::endl;
     
     return 0;
}