import sys
sys.path.append("..")

from pycompgeom import *

p1 = getVPolygon(convex=True)
p2 = getVPolygon(convex=True)

def find_bridge(poly1, poly2, upper=True):
        
     max_p1, min_p2 = max(poly1.vertices), min(poly2.vertices)
     i, j = poly1.index(max_p1), poly2.index(min_p2)
        
     if max_p1 > min_p2:
          max_p1, min_p2 = min(poly1.vertices), max(poly2.vertices)       
          i, j = poly1.index(max_p1), poly2.index(min_p2)

     s = VSegment2.from_endpoints(p1[i], p2[j]); pause()
        
     supp_hyperplane = False
     
     Astop , Bstop = False , False; 
     
     while not supp_hyperplane:
          
          if upper:
          
               if not ccw(poly1[i], poly2[j], poly2[j+1]) and not Bstop:
                    
                    j += 1
                    s = VSegment2.from_endpoints(p1[i], p2[j]); pause()
               
               else: 
                    Bstop = True

               if not ccw(poly2[j], poly1[i], poly1[i+1]) and not Astop:
                    
                    i += 1
                    s = VSegment2.from_endpoints(p1[i], p2[j]); pause()
               
               else: 
                    Astop = True
                    
          else:
               
               if ccw(poly1[i], poly2[j], poly2[j-1]) and not Bstop:
                    
                    j -= 1
                    s = VSegment2.from_endpoints(p1[i], p2[j]); pause()
               
               else: 
                    Bstop = True

               if ccw(poly2[j], poly1[i], poly1[i-1]) and not Astop:
                    
                    i -= 1
                    s = VSegment2.from_endpoints(p1[i], p2[j]); pause()
               
               else: 
                    Astop = True
               
          supp_hyperplane = Astop and Bstop

     
     return Segment2(poly1[i], poly2[j])

supp_hyperplane1 = VSegment2(find_bridge(p1, p2, True), color=BLUE)
supp_hyperplane2 = VSegment2(find_bridge(p1, p2, False), color=GREEN)

print(supp_hyperplane1.start); print(supp_hyperplane1.end) 
print(supp_hyperplane2.start); print(supp_hyperplane2.end)

pause()
pygame.quit()

