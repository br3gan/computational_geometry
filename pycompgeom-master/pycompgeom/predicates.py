def area2(p, q, r):
	return (r.y-p.y) * (q.x-p.x) - (q.y-p.y) * (r.x-p.x)

orientation = area2

def ccw(p, q, r):
	return area2(p, q, r) > 0
	
def ccwon(p, q, r):
	return area2(p, q, r) >= 0
	
def cw(p, q, r):
	return area2(p, q, r) < 0
	
def cwon(p, q, r):
	return area2(p, q, r) <= 0
	
def collinear(p, q, r):
	return area2(p, q, r) == 0
	
def between(p, q, r):
	if not collinear(p, q, r):
		return False
	if p.x != q.x:
		return p.x <= r.x <= q.x or p.x >= r.x >= q.x
	else:
		return p.y <= r.y <= q.y or p.y >= r.y >= q.y

def intersect(segment1, segment2):
        a, b = segment1.start, segment1.end
        c, d = segment2.start, segment2.end
        return ccwon(a, b, c) and cwon(a, b, d) and ccwon(c, d, b) and cwon(c, d, a) or \
               cwon(a, b, c) and ccwon(a, b, d) and cwon(c, d, b) and ccwon(c, d, a)
