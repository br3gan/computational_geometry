#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <stdio.h>

#include <fstream>  
#include <iterator>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Projection_traits_yz_3.h>
#include <CGAL/convex_hull_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K3;
typedef CGAL::Projection_traits_yz_3<K3> K;
typedef K::Point_2 Point_2;

//Program requires the existence of a file named "in.txt" with content the coordinates of the points
//It outputs a file named "out.text" with the coordinates of the Convex Hull

int main()
{ 
  int fd_i , fd_o;

  fd_i=open("in.txt", O_RDONLY );
  if( fd_i < 0 ){
     printf("file 'in.txt' not created\n");
     exit(1);
  }
  
  fd_o=open("out.txt", O_WRONLY | O_CREAT , 0644 );
    if( fd_o < 0 ){
       
     printf("file 'out.txt' not created\n");
     exit(1);
  }
  
  
  dup2(fd_i , 0);
  dup2(fd_o , 1);
  
  std::istream_iterator< Point_2 >  input_begin( std::cin );
  std::istream_iterator< Point_2 >  input_end;
  std::ostream_iterator< Point_2 >  output( std::cout, "\n" );
  CGAL::convex_hull_2( input_begin, input_end, output, K() );
  
  close( fd_i );
  close( fd_o );
  
  return 0;
}
