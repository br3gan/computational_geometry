/********************************************************************************
** Form generated from reading UI file 'Convex_hull.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONVEX_HULL_H
#define UI_CONVEX_HULL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGraphicsView>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QSplitter>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ConvexHull
{
public:
    QAction *actionQuit;
    QAction *actionInsertPoint;
    QAction *actionClear;
    QAction *actionLoadPoints;
    QAction *actionSavePoints;
    QAction *actionComputeConvexHull;
    QAction *actionInsertRandomPoints;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QSplitter *splitter;
    QGraphicsView *graphicsView;
    QPlainTextEdit *textLog;
    QMenuBar *menubar;
    QMenu *menuTools;
    QMenu *menuFile;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *ConvexHull)
    {
        if (ConvexHull->objectName().isEmpty())
            ConvexHull->setObjectName(QString::fromUtf8("ConvexHull"));
        ConvexHull->resize(1024, 768);
        actionQuit = new QAction(ConvexHull);
        actionQuit->setObjectName(QString::fromUtf8("actionQuit"));
        actionInsertPoint = new QAction(ConvexHull);
        actionInsertPoint->setObjectName(QString::fromUtf8("actionInsertPoint"));
        actionInsertPoint->setCheckable(true);
        actionInsertPoint->setChecked(false);
        actionInsertPoint->setEnabled(true);
        actionClear = new QAction(ConvexHull);
        actionClear->setObjectName(QString::fromUtf8("actionClear"));
        actionLoadPoints = new QAction(ConvexHull);
        actionLoadPoints->setObjectName(QString::fromUtf8("actionLoadPoints"));
        actionSavePoints = new QAction(ConvexHull);
        actionSavePoints->setObjectName(QString::fromUtf8("actionSavePoints"));
        actionComputeConvexHull = new QAction(ConvexHull);
        actionComputeConvexHull->setObjectName(QString::fromUtf8("actionComputeConvexHull"));
        actionComputeConvexHull->setChecked(false);
        actionInsertRandomPoints = new QAction(ConvexHull);
        actionInsertRandomPoints->setObjectName(QString::fromUtf8("actionInsertRandomPoints"));
        centralwidget = new QWidget(ConvexHull);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        splitter = new QSplitter(centralwidget);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        graphicsView = new QGraphicsView(splitter);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(graphicsView->sizePolicy().hasHeightForWidth());
        graphicsView->setSizePolicy(sizePolicy);
        graphicsView->setMinimumSize(QSize(1251, 0));
        graphicsView->setFocusPolicy(Qt::StrongFocus);
        graphicsView->setAcceptDrops(true);
        graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        graphicsView->setInteractive(true);
        graphicsView->setTransformationAnchor(QGraphicsView::NoAnchor);
        splitter->addWidget(graphicsView);
        textLog = new QPlainTextEdit(splitter);
        textLog->setObjectName(QString::fromUtf8("textLog"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(textLog->sizePolicy().hasHeightForWidth());
        textLog->setSizePolicy(sizePolicy1);
        textLog->setAcceptDrops(false);
        textLog->setReadOnly(true);
        splitter->addWidget(textLog);

        verticalLayout->addWidget(splitter);

        ConvexHull->setCentralWidget(centralwidget);
        menubar = new QMenuBar(ConvexHull);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1276, 22));
        menuTools = new QMenu(menubar);
        menuTools->setObjectName(QString::fromUtf8("menuTools"));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        ConvexHull->setMenuBar(menubar);
        statusbar = new QStatusBar(ConvexHull);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        ConvexHull->setStatusBar(statusbar);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuTools->menuAction());
        menuTools->addAction(actionInsertPoint);
        menuTools->addAction(actionInsertRandomPoints);
        menuTools->addSeparator();
        menuTools->addAction(actionComputeConvexHull);
        menuFile->addSeparator();
        menuFile->addAction(actionClear);
        menuFile->addAction(actionLoadPoints);
        menuFile->addAction(actionSavePoints);
        menuFile->addSeparator();
        menuFile->addAction(actionQuit);

        retranslateUi(ConvexHull);

        QMetaObject::connectSlotsByName(ConvexHull);
    } // setupUi

    void retranslateUi(QMainWindow *ConvexHull)
    {
        ConvexHull->setWindowTitle(QApplication::translate("ConvexHull", "Demo", 0, QApplication::UnicodeUTF8));
        actionQuit->setText(QApplication::translate("ConvexHull", "&Quit", 0, QApplication::UnicodeUTF8));
        actionQuit->setShortcut(QApplication::translate("ConvexHull", "Ctrl+Q", 0, QApplication::UnicodeUTF8));
        actionInsertPoint->setText(QApplication::translate("ConvexHull", "&Insert Point", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionInsertPoint->setToolTip(QApplication::translate("ConvexHull", "Insert Point", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionInsertPoint->setStatusTip(QApplication::translate("ConvexHull", "Left: Insert point", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_STATUSTIP
        actionClear->setText(QApplication::translate("ConvexHull", "&Clear", 0, QApplication::UnicodeUTF8));
        actionClear->setShortcut(QApplication::translate("ConvexHull", "Ctrl+C", 0, QApplication::UnicodeUTF8));
        actionLoadPoints->setText(QApplication::translate("ConvexHull", "&Load Points...", 0, QApplication::UnicodeUTF8));
        actionLoadPoints->setShortcut(QApplication::translate("ConvexHull", "Ctrl+L", 0, QApplication::UnicodeUTF8));
        actionSavePoints->setText(QApplication::translate("ConvexHull", "&Save Points...", 0, QApplication::UnicodeUTF8));
        actionSavePoints->setShortcut(QApplication::translate("ConvexHull", "Ctrl+S", 0, QApplication::UnicodeUTF8));
        actionComputeConvexHull->setText(QApplication::translate("ConvexHull", "Convex Hull", 0, QApplication::UnicodeUTF8));
        actionInsertRandomPoints->setText(QApplication::translate("ConvexHull", "&Insert Random Points", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        actionInsertRandomPoints->setToolTip(QApplication::translate("ConvexHull", "Insert Random Points", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        textLog->setPlainText(QString());
        menuTools->setTitle(QApplication::translate("ConvexHull", "&Tools", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("ConvexHull", "&File", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ConvexHull: public Ui_ConvexHull {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONVEX_HULL_H
