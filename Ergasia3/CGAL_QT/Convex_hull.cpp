#include <fstream>
#include <cmath>

// CGAL headers
#include <CGAL/Cartesian.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/convex_hull_2.h>
#include <CGAL/point_generators_2.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Real_timer.h>

// Qt headers
#include <QtGui>
#include <QString>
#include <QActionGroup>
#include <QFileDialog>
#include <QInputDialog>
#include <QGraphicsRectItem>

// GraphicsView items and event filters (input classes)

#include <CGAL/Qt/PointsGraphicsItem.h>
#include <CGAL/Qt/PolygonGraphicsItem.h>
#include <CGAL/Qt/GraphicsViewPolylineInput.h>

// for viewportsBbox
#include <CGAL/Qt/utility.h>

// the two base classes
#include "ui_Convex_hull.h"   //Ayto prepei na tropopoih8ei katallhla analoga me to onoma toy arxeioy
#include <CGAL/Qt/DemosMainWindow.h>


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::Point_2 Point_2;
typedef K::Iso_rectangle_2 Iso_rectangle_2;

typedef CGAL::Polygon_2<K> Polygon_2;

class MainWindow :
  public CGAL::Qt::DemosMainWindow,
  public Ui::ConvexHull
{
  Q_OBJECT
  
private: // Data Memebers
  
	// Scene
  QGraphicsScene scene;  

	// Models
  Polygon_2 convex_hull;
  std::vector<Point_2> points;
	
	// Graphics Items
  CGAL::Qt::PointsGraphicsItem<std::vector<Point_2> > * pgi;
  CGAL::Qt::PolygonGraphicsItem<Polygon_2> * convex_hull_gi;
  
	// Input Filters
  CGAL::Qt::GraphicsViewPolylineInput<K> * pi;
	
private: // Functions
	
	void computeConvexHull();
	void writeTextLog(const QString&);
	
	void clearPoints();
	void clearConvexHull();
	void clearTextLog();
	
public:
	
  MainWindow();

public slots:

  void processInput(CGAL::Object o);

  void on_actionComputeConvexHull_triggered();

  void on_actionInsertPoint_toggled(bool checked);
  
  void on_actionInsertRandomPoints_triggered();

  void on_actionLoadPoints_triggered();

  void on_actionSavePoints_triggered();

  void on_actionClear_triggered();

  virtual void open(QString fileName);

signals:
	
	void pointsChanged();
  void convexHullChanged();
};


MainWindow::MainWindow()
  : DemosMainWindow()
{
  setupUi(this);

  // Graphics Item for the input point set
  pgi = new CGAL::Qt::PointsGraphicsItem<std::vector<Point_2> >(&points);
	QObject::connect(this, SIGNAL(pointsChanged()), 
									 pgi, SLOT(modelChanged()));
  pgi->setVerticesPen(QPen(Qt::black, 4, Qt::SolidLine, 
													 Qt::RoundCap, Qt::RoundJoin));
  scene.addItem(pgi);

  // Graphics Item for the convex hull
  convex_hull_gi = new CGAL::Qt::PolygonGraphicsItem<Polygon_2>(&convex_hull);
	QObject::connect(this, SIGNAL(convexHullChanged()), 
									 convex_hull_gi, SLOT(modelChanged()));
  convex_hull_gi->setEdgesPen(QPen(Qt::black, 0, Qt::SolidLine, 
																	 Qt::RoundCap, Qt::RoundJoin));
  scene.addItem(convex_hull_gi);

  // Setup input handlers. They get events before the scene gets them
  // and the input they generate is passed to GraphicsViewPolylineInput with 
  // the signal/slot mechanism    
  pi = new CGAL::Qt::GraphicsViewPolylineInput<K>(this, &scene, 1);
	QObject::connect(pi, SIGNAL(generate(CGAL::Object)), 
									 this, SLOT(processInput(CGAL::Object)));

  // 
  // Manual handling of actions
  //

  QObject::connect(this->actionQuit, SIGNAL(triggered()), 
		   this, SLOT(close()));

  //
  // Setup the scene and the view
  //
  scene.setItemIndexMethod(QGraphicsScene::NoIndex);
  scene.setSceneRect(-200, -200, 200, 200);
  // graphicsView inherited from Ui::Bounding_volumes
  this->graphicsView->setScene(&scene);
  this->graphicsView->setMouseTracking(true);

  // Turn the vertical axis upside down
	//this->graphicsView->matrix().scale(1, -1);
  this->graphicsView->scale(1, -1);
                                                      
  // The navigation adds zooming and translation functionality to the
  // QGraphicsView:
  // addNavigation() (implemented in DemosMainWindow)
  // adds a CGAL::Qt::GraphicsViewNavigation object as a navigation filter 
  // to graphicsView (inherited member of Ui::Bounding_volumes)
  // All the following functions and signal openRecentFile() are implemented in DemosMainWindow
  this->addNavigation(this->graphicsView);

  this->setupStatusBar();
  this->setupOptionsMenu();

  this->addRecentFiles(this->menuFile, this->actionQuit);
  connect(this, SIGNAL(openRecentFile(QString)), this, SLOT(open(QString)));
}

void
MainWindow::computeConvexHull(){
	// Time measurements
	CGAL::Real_timer t;
	t.start();
	// Compute Convex Hull
	CGAL::convex_hull_2(points.begin(), points.end(), std::back_inserter(convex_hull));
	t.stop();
	// Prepare Qstring for textLog
	QString str = QString("#points in convex hull: %1\nreal time elapsed: %2 seconds\n")
								.arg(convex_hull.size()).arg(t.time());
	writeTextLog(str);
}

void
MainWindow::writeTextLog(const QString& s){
	// setPlainText discards previous content
	this->textLog->setPlainText(s);
}

void
MainWindow::clearPoints(){
	points.clear();
}

void 
MainWindow::clearConvexHull(){
	convex_hull.clear();
}

void
MainWindow::clearTextLog(){
	this->textLog->clear();
}

void
MainWindow::processInput(CGAL::Object o)
{
	//std::cout << "inside processInput.." << std::endl;
	
  std::list<Point_2> input;
  if(CGAL::assign(input, o)){
    Point_2 p = input.front();
    points.push_back(p);
		emit(pointsChanged());
	}
}


/* 
 *  Qt Automatic Connections
 *  http://doc.trolltech.com/4.4/designer-using-a-component.html#automatic-connections
 * 
 *  setupUi(this) generates connections to the slots named
 *  "on_<action_name>_<signal_name>"
 */

void
MainWindow::on_actionInsertPoint_toggled(bool checked)
{
  if(checked){
    scene.installEventFilter(pi);
  } else {
    scene.removeEventFilter(pi);
  }
}

void
MainWindow::on_actionComputeConvexHull_triggered()
{
	clearConvexHull();
	computeConvexHull();
  
	emit(convexHullChanged());
}

void
MainWindow::on_actionClear_triggered()
{
  clearPoints();
  clearConvexHull();
	clearTextLog();
  
  emit(pointsChanged());
	emit(convexHullChanged());
}

void
MainWindow::on_actionInsertRandomPoints_triggered()
{
  QRectF rect = CGAL::Qt::viewportsBbox(&scene);
  CGAL::Qt::Converter<K> convert;  
  Iso_rectangle_2 isor = convert(rect);
  CGAL::Random_points_in_iso_rectangle_2<Point_2> pg((isor.min)(), (isor.max)());
  bool ok = false;
  const int number_of_points = 
	QInputDialog::getInteger(this, 
													 tr("Number of random points"),
													 tr("Enter number of random points"),
													 100,
													 0,
													 (std::numeric_limits<int>::max)(),
													 1,
													 &ok);

  if(!ok) {
    return;
  }

  // wait cursor
  QApplication::setOverrideCursor(Qt::WaitCursor);
  for(int i = 0; i < number_of_points; ++i){
    Point_2 p = *pg++;
    points.push_back(p);
  }

  // default cursor
  QApplication::restoreOverrideCursor();
  emit(pointsChanged());
}


void
MainWindow::on_actionLoadPoints_triggered()
{
  QString fileName = QFileDialog::getOpenFileName(this,
						  tr("Open Points file"),
						  ".");
  if(! fileName.isEmpty()){
    open(fileName);
  }
}


void
MainWindow::open(QString fileName)
{
  // wait cursor
  QApplication::setOverrideCursor(Qt::WaitCursor);
  std::ifstream ifs(qPrintable(fileName));
  
	clearPoints();
  clearConvexHull();
	clearTextLog();
	
  K::Point_2 p;
  while(ifs >> p) {
    points.push_back(p);
  }
	
  // default cursor
  QApplication::restoreOverrideCursor();
  this->addToRecentFiles(fileName);
	
  emit(pointsChanged());
	emit(convexHullChanged());
    
}

void
MainWindow::on_actionSavePoints_triggered()
{
  QString fileName = QFileDialog::getSaveFileName(this,
						  tr("Save points"),
						  ".");
  if(! fileName.isEmpty()){
    std::ofstream ofs(qPrintable(fileName));
    for(std::vector<Point_2>::const_iterator  
          vit = points.begin(),
          end = points.end();
        vit!= end; ++vit)
    {
      ofs << *vit << std::endl;
    }
  }
}

#include "Convex_hull.moc"  //ayto prepei na tropopoih8ei analoga me to onoma toy arxeioy

int main(int argc, char **argv)
{
  QApplication app(argc, argv);

  MainWindow mainWindow;
  mainWindow.show();
  return app.exec();
}

