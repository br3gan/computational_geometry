#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include <CGAL/Polygon_2.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <unistd.h>

//Colors
#define RED 1
#define BLUE 2
#define YELLOW 3

//Edges
#define FIRST_VERTEX 0
#define SECOND_VERTEX 1
#define THIRD_VERTEX 2

//Structs to save Face info
struct Face
{
     Face(){}
     int is_overlapping;
     bool complete;
     
     bool in_hull(){
          return is_overlapping%2 == 1;
     }
     bool is_complete(){
          return complete;
     }
};

//Struct to save Vertex info
struct Vertex
{
     
     Vertex(){}
     //1 - red , 2 - blue , 3 - yellow
     int color;
     //true if edge is colored
     bool is_colored;
     //unique vertex id 
     int vertex_id;
     
     int get_color(){
          return color;
     }
     bool get_colored(){
          return is_colored;
     }
     int get_v_id(){
          return vertex_id;
     }
};

typedef CGAL::Exact_predicates_inexact_constructions_kernel       K;
typedef CGAL::Triangulation_vertex_base_with_info_2<Vertex,K> Vbb;
typedef CGAL::Triangulation_face_base_with_info_2<Face,K>    Fbb;
typedef CGAL::Constrained_triangulation_face_base_2<K,Fbb>        Fb;
typedef CGAL::Triangulation_data_structure_2<Vbb,Fb>              TDS;
typedef CGAL::Exact_predicates_tag                                Itag;
typedef CGAL::Constrained_Delaunay_triangulation_2<K, TDS, Itag>  CDT;
typedef CDT::Point                                                Point;
typedef CGAL::Polygon_2<K>                                        Polygon_2;
typedef CDT::Face_iterator Face_iterator;

//We have to remove extra edges and keep the hull of the non-convex polygon
//**code from CGAL libraly: Triangulation_2/polygon_triangulation.cpp**
void save_hull(CDT& ct, CDT::Face_handle start, int index, std::list<CDT::Edge>& border ){
     
     if(start->info().is_overlapping != -1){
          return;
     }
     std::list<CDT::Face_handle> queue;
     queue.push_back(start);
     
     while(! queue.empty()){
          CDT::Face_handle fh = queue.front();
          queue.pop_front();
          if(fh->info().is_overlapping == -1){
               fh->info().is_overlapping = index;
               for(int i = FIRST_VERTEX; i <= THIRD_VERTEX; i++){
                    CDT::Edge e(fh,i);
                    CDT::Face_handle n = fh->neighbor(i);
                    if(n->info().is_overlapping == -1){
                         if(ct.is_constrained(e)) border.push_back(e);
                         else queue.push_back(n);
                    }
               }
          }
     }
}

void save_hull(CDT& cdt)
{
     for(CDT::All_faces_iterator it = cdt.all_faces_begin(); it != cdt.all_faces_end(); ++it){
          it->info().is_overlapping = -1;
     }
     
     int index = 0;
     std::list<CDT::Edge> border;
     save_hull(cdt, cdt.infinite_face(), index++, border);
     while(! border.empty()){
          CDT::Edge e = border.front();
          border.pop_front();
          CDT::Face_handle n = e.first->neighbor(e.second);
          if(n->info().is_overlapping == -1){
               save_hull(cdt, n, e.first->info().is_overlapping + 1, border);
          }
     }
}

//constrained triangulation of polygon
void insert_polygon(CDT& cdt,const Polygon_2& polygon){
     if ( polygon.is_empty() ) return;
     CDT::Vertex_handle v_prev=cdt.insert(*CGAL::cpp0x::prev(polygon.vertices_end()));
     for (Polygon_2::Vertex_iterator vit=polygon.vertices_begin();
          vit!=polygon.vertices_end();++vit)
          {
               CDT::Vertex_handle vh=cdt.insert(*vit);
               cdt.insert_constraint(vh,v_prev);
               v_prev=vh;
          }
}

//Returns 0 if vertex id is not inside the vector
//Returns 1 if vertex id exists inside the vextor
bool element_inside_vector(int v_id , std::vector<int> id_and_color){
     int count = 0;
     for (int i = 0; i <= id_and_color.size(); i++) {
          if( v_id == id_and_color[i]){
               count++;
               break;
          }
     }
     
     if(count > 0){
          return true;
     }else{
          return false;
     }
}

//colorize a yet non visited vertex 
void color_vertex( CDT::Finite_faces_iterator& it , std::vector<int>& color , int edge , int color_id ){
     
     if (element_inside_vector(it->vertex(edge)->info().vertex_id, color) == false) {
          it->vertex(edge)->info().color = color_id;
          color.push_back(it->vertex(edge)->info().vertex_id);
          it->info().complete = true;
          return;
     }
     it->info().complete = true;
     return;
}

//colorize the vertex of an edge
int handle_edges( CDT::Finite_faces_iterator& it , std::vector<int>& one_color , std::vector<int>& other_color , int one_color_id ,int other_color_id , int one_vertex , int other_vertex ){
     
     if (element_inside_vector(it->vertex(one_vertex)->info().vertex_id, one_color) == true) {
          
          color_vertex( it , other_color , other_vertex , other_color_id );
          return 1;
          
     }else if(element_inside_vector(it->vertex(one_vertex)->info().vertex_id, other_color) == true){
          
          color_vertex( it , one_color , other_vertex , one_color_id);
          return 1;
          
     }else if(element_inside_vector(it->vertex(other_vertex)->info().vertex_id, one_color) == true){
          
          color_vertex( it , other_color , one_vertex , other_color_id );
          return 1;
          
     }else if(element_inside_vector(it->vertex(other_vertex)->info().vertex_id, other_color) == true){
          
          color_vertex( it , one_color , one_vertex , one_color_id );
          return 1;
     }
     
     return 0;
}

//chooses what color is the least displayed and returns its code
int pick_color( int red , int blue , int yellow){
     
     int min = red;
     
     if (blue < min)
          min = blue;
     
     if (yellow < min )
          min = yellow;
     
     if( min == red )
          return RED;
     if( min == blue )
          return BLUE;
     
     return YELLOW; 
}

//prints the verices of the positioned guards
void print_guards( std::vector<int>& color , CDT& cdt , std::ofstream& ofs){
     
     for (int i = 0; i < color.size(); i++) {
          bool get_me_out = false;
          for (CDT::Finite_faces_iterator it=cdt.finite_faces_begin();
               it!=cdt.finite_faces_end();++it)
               {
                    if ( it->info().in_hull() ){
                         
                         for (int k=FIRST_VERTEX; k <= THIRD_VERTEX; k++) {
                              
                              if (color.at(i) == it->vertex(k)->info().get_v_id() ) {
                                   std::cout << it->vertex(k)->point() << std::endl;
                                   ofs  << it->vertex(k)->point() << std::endl;
                                   get_me_out = true;
                              }
                         }
                    }
                    if(get_me_out == true){
                         break;
                    }
               }
     }
}

//initialize the id and infos of the vertices 
int initialization( CDT& cdt , int& facelets_count , int& vertex_id ){
     
     for (CDT::Finite_faces_iterator it=cdt.finite_faces_begin(); it!=cdt.finite_faces_end(); ++it){
          if ( it->info().in_hull() ){
               it->info().complete = false;
               
               for( int edge = FIRST_VERTEX ; edge <= THIRD_VERTEX ; edge++ ){
                    it->vertex(edge)->info().is_colored = false;
                    it->vertex(edge)->info().vertex_id = vertex_id++;
               }
               facelets_count++;
          }
     }
     
     return facelets_count;
}

//read the points from file, returns tha polygon
Polygon_2 read_points( char* filename){
     
     //input stream
     std::ifstream ifs (filename, std::ifstream::in);
     
     char buffer[32] ; int i = 0;
     double vertices[2] ; int v = 0;
     
     //initialize buffers
     for( i = 0 ; i < 2 ; i++){
          vertices[i] = 0.0;
     }
     for( i = 0 ; i < 32 ; i++){
          buffer[i] = '\0';
     }
     
     i = 0;
     
     Polygon_2 polygon1;
     
     char c = ifs.get();
     
     //Read Points
     while (ifs.good()) {
          v = 0; 
          while( c != '\n' && c != EOF ){
               i = 0;
               while( c != ' ' && c != '\n' && c != EOF){
                    
                    buffer[i++] = c;
                    c = ifs.get();
               }

               vertices[v++] = atof(buffer);
               while( i >= 0 )
                    buffer[i--]='\0';
               
               if( c == '\n' )
                    break;
               c = ifs.get();
          }
          polygon1.push_back(Point(vertices[0] * 0.5 , vertices[1] * 0.5 ));
          while(v>=0)
               vertices[v--] = 0.0;
          c = ifs.get();
     }
     
     //close input file
     ifs.close();
     return polygon1;
}

//print the color from its code
std::string print_color( int color ){
     
     if (color == 1)
          return "red";
     if (color == 2)
          return "blue";
     if (color == 3)
          return "yellow"; 
}

int main( int argc , char* argv[] ){
     
     if( argc != 2 ){
          std::cout << "Expected input:: \n [Arg1 : input_filename]" << std::endl;
          return 0;
     }
     
     //output stream
     std::ofstream ofs;
     ofs.open ("results.txt");
     
     //start from fresh file
     ofs < "";
     
     Polygon_2 polygon1;
     
     polygon1 = read_points( argv[1] );

     CDT cdt;
     insert_polygon(cdt,polygon1);
     save_hull(cdt);

     int vertex_id = 1;
     int facelets_count = 0;
     
     //initialize vertex info, find how many facelets we created
     initialization( cdt , facelets_count , vertex_id );
     
     int count=0;
     
     //create vectors for each color 
     std::vector<int> red;
     std::vector<int> blue;
     std::vector<int> yellow;
     for (int facelets = 0; facelets<=facelets_count; facelets++) {
          
          //3-Color edges
          for (CDT::Finite_faces_iterator it=cdt.finite_faces_begin(); it!=cdt.finite_faces_end();++it )
               {
                    if ( it->info().in_hull() ){
                         
                         //if the triangle is not visited, colorize all the edges
                         if(count == 0){ 
                              it->vertex(FIRST_VERTEX)->info().color = RED;
                              red.push_back(it->vertex(FIRST_VERTEX)->info().vertex_id);
                              it->vertex(SECOND_VERTEX)->info().color = BLUE;
                              blue.push_back(it->vertex(SECOND_VERTEX)->info().vertex_id);
                              it->vertex(THIRD_VERTEX)->info().color = YELLOW;
                              yellow.push_back(it->vertex(THIRD_VERTEX)->info().vertex_id);
                              
                              for(int vert = FIRST_VERTEX ; vert <= THIRD_VERTEX ; vert++ )
                                   it->vertex(vert)->info().is_colored = true;
                              
                              it->info().complete = true;
                              
                         }
                         //if the triangle has at least one or two uncolored verices
                         //color it/them according to the following rules 
                         else{
                              if(it->info().complete == false  ){
                                   
                                   //case first vertex = blue
                                   if (element_inside_vector(it->vertex(FIRST_VERTEX)->info().vertex_id, blue) == true) {
                                        
                                        if (handle_edges( it , red , yellow , RED , YELLOW , SECOND_VERTEX , THIRD_VERTEX ) == 1 )
                                             continue;
                                   }
                                   //case first vertex = red
                                   if (element_inside_vector(it->vertex(FIRST_VERTEX)->info().vertex_id, red) == true) {
                                        
                                        if (handle_edges( it , blue , yellow , BLUE , YELLOW , SECOND_VERTEX , THIRD_VERTEX ) == 1 )
                                             continue;
                                   }
                                   //case first vertex = yellow
                                   if (element_inside_vector(it->vertex(FIRST_VERTEX)->info().vertex_id, yellow) == true) {
                                        
                                        if (handle_edges( it , red , blue , RED , BLUE , SECOND_VERTEX , THIRD_VERTEX ) == 1 )
                                             continue;
                                   }
                                   //case second vertex = blue
                                   else if(element_inside_vector(it->vertex(SECOND_VERTEX)->info().vertex_id, blue) == true){
                                        
                                        if (handle_edges( it , red , yellow , RED , YELLOW , FIRST_VERTEX , THIRD_VERTEX ) == 1 )
                                             continue;
                                   //case second vertex = red    
                                   }else if(element_inside_vector(it->vertex(SECOND_VERTEX)->info().vertex_id, red) == true){
                                        
                                        if (handle_edges( it , blue , yellow , BLUE , YELLOW , FIRST_VERTEX , THIRD_VERTEX ) == 1 )
                                             continue;
                                        
                                   }
                                   //case second vertex = yellow
                                   if(element_inside_vector(it->vertex(SECOND_VERTEX)->info().vertex_id, yellow) == true){
                                        
                                        if (handle_edges( it , red , blue , RED , BLUE , FIRST_VERTEX , THIRD_VERTEX ) == 1 )
                                             continue;
                                        
                                   }
                                   //case third vertex = blue
                                   if(element_inside_vector(it->vertex(THIRD_VERTEX)->info().vertex_id, blue) == true){
                                        
                                        if (handle_edges( it , red , yellow , RED , YELLOW , SECOND_VERTEX , FIRST_VERTEX ) == 1 )
                                             continue;
                                   }
                                   //case third vertex = red
                                   if(element_inside_vector(it->vertex(THIRD_VERTEX)->info().vertex_id, red) == true){
                                        
                                        if (handle_edges( it , blue , yellow , BLUE , YELLOW , SECOND_VERTEX , FIRST_VERTEX ) == 1 )
                                             continue;
                                        
                                   }
                                   //case third vertex = yellow
                                   if(element_inside_vector(it->vertex(THIRD_VERTEX)->info().vertex_id, yellow) == true){
                                        
                                        if (handle_edges( it , red , blue , RED , BLUE , SECOND_VERTEX , FIRST_VERTEX ) == 1 )
                                             continue;
                                        
                                   }
                              }
                         }
                         count++;
                    }
               }
     }
     
     int triangle = 0;
     
     for (CDT::Finite_faces_iterator it = cdt.finite_faces_begin(); it!=cdt.finite_faces_end() ; ++it){
          if ( it->info().in_hull() ){
               std::cout << "triangle " << triangle + 1 << "/" << facelets_count << ":" << std::endl;
               std::cout << "(" << it->vertex(FIRST_VERTEX)->point() << ") | color: " << print_color( it->vertex(FIRST_VERTEX)->info().get_color() ) << std::endl;
               std::cout << "(" << it->vertex(SECOND_VERTEX)->point() << ") | color: " << print_color( it->vertex(SECOND_VERTEX)->info().get_color() ) << std::endl;
               std::cout << "(" << it->vertex(THIRD_VERTEX)->point() << ") | color: " << print_color( it->vertex(THIRD_VERTEX)->info().get_color() ) << std::endl;
               std::cout <<"......................................................................" << std::endl;
               triangle++;
          }
     }
     
     //find the least displayed color
     int selected = pick_color( red.size() , blue.size() , yellow.size() );
     
     std::cout << "Selected color is " << print_color(selected) << std::endl;
     
     if(selected == RED){
          std::cout << "Guards positions: " << std::endl;
          print_guards(red , cdt , ofs);
     }else if(selected == BLUE){
          std::cout << "Guards positions: " << std::endl;
          print_guards(blue , cdt , ofs);
     }else if(selected == YELLOW){
          std::cout << "Guards positions:" << std::endl;
          print_guards(yellow , cdt , ofs);
     }
     
     std::cout << "================================================"<< std::endl;
     std::cout << "Triangulation created:"<< std::endl;
     std::cout << red.size() << " red vertices" << std::endl;
     std::cout << blue.size() << " blue vertices" <<  std::endl;
     std::cout <<  yellow.size() << " yellow vetices" << std::endl << std::endl;;
     std::cout << "and " <<  facelets_count << " facets." << std::endl;
     
     ofs << "*" << std::endl;
     for( int i = 0 ; i < polygon1.size() ; i++ )
          ofs << polygon1[i] << std::endl;
     
     ofs.close();
     
     if ( execlp( "./Dialog/Dialog" , "Dialog" , NULL ) == -1 )
          std::cout << "NO EXEC OCCURED" << std::endl;
}