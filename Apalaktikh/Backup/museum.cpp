#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include <CGAL/Polygon_2.h>
#include <vector>
#include <iostream>
#include <graphics.h>

#include <QApplication.h>
#include <QWidget/h>

//Colors
#define RED 1
#define BLUE 2
#define YELLOW 3

//Edges
#define FIRST_VERTEX 0
#define SECOND_VERTEX 1
#define THIRD_VERTEX 2


/*oi domes stis opoies apothikevode oi plirofories gia ta faces kai ta vertices*/

struct Face
{
     Face(){}
     int is_overlapping;
     bool complete;
     
     bool in_hull(){
          return is_overlapping%2 == 1;
     }
     bool is_complete(){
          return complete;
     }
};

struct Vertex{
     
     Vertex(){}
     int color; // 1 = Kokkino, 2 = Mple, 3 = Prasino
     bool is_colored; /* simbolizei to an exei xrwmatistei i korifi */
     int vertex_id;  /* simbolizei to vertex_id tou kathe vertex,to opoio einai monadiko */
     
     int get_color(){
          return color;
     }
     bool get_colored(){
          return is_colored;
     }
     int get_v_id(){
          return vertex_id;
     }
};

typedef CGAL::Exact_predicates_inexact_constructions_kernel       K;
typedef CGAL::Triangulation_vertex_base_with_info_2<Vertex,K> Vbb;
typedef CGAL::Triangulation_face_base_with_info_2<Face,K>    Fbb;
typedef CGAL::Constrained_triangulation_face_base_2<K,Fbb>        Fb;
typedef CGAL::Triangulation_data_structure_2<Vbb,Fb>              TDS;
typedef CGAL::Exact_predicates_tag                                Itag;
typedef CGAL::Constrained_Delaunay_triangulation_2<K, TDS, Itag>  CDT;
typedef CDT::Point                                                Point;
typedef CGAL::Polygon_2<K>                                        Polygon_2;
typedef CDT::Face_iterator Face_iterator;

/* i trigonopiisi enos aplou poligonou odigei sekirto polugwno. Synepws xrisimopoioume aftes tis
 *  duo sinartiseis gia na aferesoume ta trigwna ta opoia dn apoteloun meros tu aplou polugwnou.*/

void save_hull(CDT& ct, CDT::Face_handle start, int index, std::list<CDT::Edge>& border ){
     
     if(start->info().is_overlapping != -1){
          return;
     }
     std::list<CDT::Face_handle> queue;
     queue.push_back(start);
     
     while(! queue.empty()){
          CDT::Face_handle fh = queue.front();
          queue.pop_front();
          if(fh->info().is_overlapping == -1){
               fh->info().is_overlapping = index;
               for(int i = FIRST_VERTEX; i <= THIRD_VERTEX; i++){
                    CDT::Edge e(fh,i);
                    CDT::Face_handle n = fh->neighbor(i);
                    if(n->info().is_overlapping == -1){
                         if(ct.is_constrained(e)) border.push_back(e);
                         else queue.push_back(n);
                    }
               }
          }
     }
}


void save_hull(CDT& cdt)
{
     for(CDT::All_faces_iterator it = cdt.all_faces_begin(); it != cdt.all_faces_end(); ++it){
          it->info().is_overlapping = -1;
     }
     
     int index = FIRST_VERTEX;
     std::list<CDT::Edge> border;
     save_hull(cdt, cdt.infinite_face(), index++, border);
     while(! border.empty()){
          CDT::Edge e = border.front();
          border.pop_front();
          CDT::Face_handle n = e.first->neighbor(e.second);
          if(n->info().is_overlapping == -1){
               save_hull(cdt, n, e.first->info().is_overlapping+1, border);
          }
     }
}

/* edw ginetai i constrained triangulation */
void insert_polygon(CDT& cdt,const Polygon_2& polygon){
     if ( polygon.is_empty() ) return;
     CDT::Vertex_handle v_prev=cdt.insert(*CGAL::cpp0x::prev(polygon.vertices_end()));
     for (Polygon_2::Vertex_iterator vit=polygon.vertices_begin();
          vit!=polygon.vertices_end();++vit)
          {
               CDT::Vertex_handle vh=cdt.insert(*vit);
               cdt.insert_constraint(vh,v_prev);
               v_prev=vh;
          }
}

/*epistrefei ean ena stixeio iparxei se ena vector. Xrisimopoithike vector gia na apothikevetai to vertex id sto xrwma pu anikei*/
bool element_inside_vector(int v_id , std::vector<int> id_and_color){
     int count = 0;
     for (int i = 0; i <= id_and_color.size(); i++) {
          if( v_id == id_and_color[i]){
               count++;
               break;
          }
     }
     
     if(count > 0){
          return true;
     }else{
          return false;
     }
}

void color_vertex( CDT::Finite_faces_iterator& fit , std::vector<int>& color , int edge , int color_id ){
     
     if (element_inside_vector(fit->vertex(edge)->info().vertex_id, color) == false) {
          fit->vertex(edge)->info().color = color_id;
          color.push_back(fit->vertex(edge)->info().vertex_id);
          fit->info().complete = true;
          return;
     }
     fit->info().complete = true;
     return;
}

int handle_edges( CDT::Finite_faces_iterator& fit , std::vector<int>& one_color , std::vector<int>& other_color , int one_color_id ,int other_color_id , int one_vertex , int other_vertex ){
     
     if (element_inside_vector(fit->vertex(one_vertex)->info().vertex_id, one_color) == true) {
          
          color_vertex( fit , other_color , other_vertex , other_color_id );
          return 1;
          
          
     }else if(element_inside_vector(fit->vertex(one_vertex)->info().vertex_id, other_color) == true){
          
          color_vertex( fit , one_color , other_vertex , one_color_id);
          return 1;
          
          
     }else if(element_inside_vector(fit->vertex(other_vertex)->info().vertex_id, one_color) == true){
          
          color_vertex( fit , other_color , one_vertex , other_color_id );
          return 1;
          
          
     }else if(element_inside_vector(fit->vertex(other_vertex)->info().vertex_id, other_color) == true){
          
          color_vertex( fit , one_color , one_vertex , one_color_id );
          return 1;
     }
     
     return 0;
     
}

int pick_color( int red , int blue , int yellow){
     
     int min = red;
     
     if (blue < min)
          min = blue;
     
     if (yellow < min )
          min = yellow;
     
     if( min == red )
          return RED;
     if( min == blue )
          return BLUE;
     
     return YELLOW; 
}

void print_guards( std::vector<int>& color , CDT& cdt ){
     
     for (int i = 0; i < color.size(); i++) {
          bool get_me_out = false;
          for (CDT::Finite_faces_iterator fit=cdt.finite_faces_begin();
               fit!=cdt.finite_faces_end();++fit)
               {
                    if ( fit->info().in_hull() ){
                         
                         for (int k=FIRST_VERTEX; k <= THIRD_VERTEX; k++) {
                              
                              if (color.at(i) == fit->vertex(k)->info().get_v_id() ) {
                                   std::cout << fit->vertex(k)->point() << std::endl;
                                   get_me_out = true;
                              }
                         }
                    }
                    if(get_me_out == true){
                         break;
                    }
               }
     }
}

int initialization( CDT& cdt , int& facelets_count , int& vertex_id ){
     
     for (CDT::Finite_faces_iterator fit=cdt.finite_faces_begin(); fit!=cdt.finite_faces_end(); ++fit){
          if ( fit->info().in_hull() ){
               std::cout << vertex_id <<  std::endl;
               fit->info().complete = false;
               
               for( int edge = FIRST_VERTEX ; edge <= THIRD_VERTEX ; edge++ ){
                    fit->vertex(edge)->info().is_colored = false;
                    fit->vertex(edge)->info().vertex_id = vertex_id++;
               }
               facelets_count++;
          }
     }
     
     return facelets_count;
}

int main( int argc , char* argv[] )
{
     /*eisagwgi simeiwn*/
     Polygon_2 polygon1;
     
     polygon1.push_back(Point(1,1));
     polygon1.push_back(Point(2,1));
     polygon1.push_back(Point(2,3));
     polygon1.push_back(Point(3,3));
     polygon1.push_back(Point(3,1));
     polygon1.push_back(Point(4,1));
     polygon1.push_back(Point(4,3));
     polygon1.push_back(Point(5,3));
     polygon1.push_back(Point(5,1));
     polygon1.push_back(Point(6,1));
     polygon1.push_back(Point(10,1));
     polygon1.push_back(Point(5,4));
     polygon1.push_back(Point(1,4));
     
     CDT cdt;
     insert_polygon(cdt,polygon1);
     save_hull(cdt);
     
     int vertex_id = 1;
     int facelets_count = 0;
     
     /*arxikopoiiseis colored, kai twn vertex vertex_id
      *    Episis vriskoume posa faces exoun dimiurgithei*/
     initialization( cdt , facelets_count , vertex_id );
     
     int count=0;
     
     /*dimiurgume ta vector*/
     std::vector<int> red;
     std::vector<int> blue;
     std::vector<int> yellow;
     for (int k=0; k<=facelets_count; k++) {
          
          //3-Color edges
          for (CDT::Finite_faces_iterator fit=cdt.finite_faces_begin();
               fit!=cdt.finite_faces_end();++fit)
               {
                    if ( fit->info().in_hull() ){
                         
                         if(count == 0){ /*An den exei mpei pote apla xromatiseta ola ta shmeia*/
                              
                              fit->vertex(FIRST_VERTEX)->info().color = RED;
                              red.push_back(fit->vertex(FIRST_VERTEX)->info().vertex_id);
                              fit->vertex(SECOND_VERTEX)->info().color = BLUE;
                              blue.push_back(fit->vertex(SECOND_VERTEX)->info().vertex_id);
                              fit->vertex(THIRD_VERTEX)->info().color = YELLOW;
                              yellow.push_back(fit->vertex(THIRD_VERTEX)->info().vertex_id);
                              
                              for(int vert = FIRST_VERTEX ; vert <= THIRD_VERTEX ; vert++ )
                                   fit->vertex(vert)->info().is_colored = true;
                              
                              fit->info().complete = true;
                              
                         }else{
                              /*An to trigwno den exei ginei complete, diladi den exei xwmatistei plirws, prospathei na brei tus sindiadsmous twn xrwmatwn
                               *                 gia na vapsei tin epomeni koryfi. Ta if xrisimopoioude gia na vrethun oloi oi sindiasmoi*/
                              
                              if(fit->info().complete == false  ){
                                   
                                   if (element_inside_vector(fit->vertex(FIRST_VERTEX)->info().vertex_id, blue) == true) {// BLUE
                                        
                                        if (handle_edges( fit , red , yellow , RED , YELLOW , SECOND_VERTEX , THIRD_VERTEX ) == 1 )
                                             continue;
                                   }
                                   if (element_inside_vector(fit->vertex(FIRST_VERTEX)->info().vertex_id, red) == true) {  // RED
                                        
                                        if (handle_edges( fit , blue , yellow , BLUE , YELLOW , SECOND_VERTEX , THIRD_VERTEX ) == 1 )
                                             continue;
                                   }
                                   if (element_inside_vector(fit->vertex(FIRST_VERTEX)->info().vertex_id, yellow) == true) { // YELLOW
                                        
                                        if (handle_edges( fit , red , blue , RED , BLUE , SECOND_VERTEX , THIRD_VERTEX ) == 1 )
                                             continue;
                                   }
                                   
                                   else if(element_inside_vector(fit->vertex(SECOND_VERTEX)->info().vertex_id, blue) == true){ //BLUE
                                        
                                        if (handle_edges( fit , red , yellow , RED , YELLOW , FIRST_VERTEX , THIRD_VERTEX ) == 1 )
                                             continue;
                                        
                                   }else if(element_inside_vector(fit->vertex(SECOND_VERTEX)->info().vertex_id, red) == true){ //RED
                                        
                                        if (handle_edges( fit , blue , yellow , BLUE , YELLOW , FIRST_VERTEX , THIRD_VERTEX ) == 1 )
                                             continue;
                                        
                                   }
                                   if(element_inside_vector(fit->vertex(SECOND_VERTEX)->info().vertex_id, yellow) == true){ //Yellow
                                        
                                        if (handle_edges( fit , red , blue , RED , BLUE , FIRST_VERTEX , THIRD_VERTEX ) == 1 )
                                             continue;
                                        
                                   }
                                   
                                   if(element_inside_vector(fit->vertex(THIRD_VERTEX)->info().vertex_id, blue) == true){ // BLUE
                                        
                                        if (handle_edges( fit , red , yellow , RED , YELLOW , SECOND_VERTEX , FIRST_VERTEX ) == 1 )
                                             continue;
                                   }
                                   
                                   if(element_inside_vector(fit->vertex(THIRD_VERTEX)->info().vertex_id, red) == true){ //RED
                                        
                                        if (handle_edges( fit , blue , yellow , BLUE , YELLOW , SECOND_VERTEX , FIRST_VERTEX ) == 1 )
                                             continue;
                                        
                                   }
                                   
                                   if(element_inside_vector(fit->vertex(THIRD_VERTEX)->info().vertex_id, yellow) == true){ // YELLOW
                                        
                                        if (handle_edges( fit , red , blue , RED , BLUE , SECOND_VERTEX , FIRST_VERTEX ) == 1 )
                                             continue;
                                        
                                   }
                              }
                         }
                         count++;
                    }
               }
     }
     
     /*Ektypwseis*/
     int zozela =0;
     
     for (CDT::Finite_faces_iterator fit=cdt.finite_faces_begin();
          fit!=cdt.finite_faces_end();++fit)
          {
               if ( fit->info().in_hull() ){
                    std::cout << zozela << std::endl;
                    //     std::cout << fit->vertex(FIRST_VERTEX)->info().get_color() << std::endl;
                    std::cout << fit->vertex(FIRST_VERTEX)->point() << " color: " << fit->vertex(FIRST_VERTEX)->info().get_color() << " - " << fit->vertex(FIRST_VERTEX)->info().get_v_id() << std::endl;
                    std::cout << fit->vertex(SECOND_VERTEX)->point() << " color: " << fit->vertex(SECOND_VERTEX)->info().get_color()  << " - " << fit->vertex(SECOND_VERTEX)->info().get_v_id() << std::endl;
                    std::cout << fit->vertex(THIRD_VERTEX)->point() << " color: " << fit->vertex(THIRD_VERTEX)->info().get_color()  << " - " << fit->vertex(THIRD_VERTEX)->info().get_v_id() << std::endl;
                    std::cout <<"/////////////////////////////////////////////////////////" << std::endl;
                    zozela++;
               }
          }
          
          /*Ipologizetai edw to xrwma pou exei xrwmatisei tis ligoteres korifes etsi wste na topothetithun saftes oi filakes.*/
          int selected = pick_color( red.size() , blue.size() , yellow.size() );
          
          if(selected == RED){
               std::cout << "Selected color is red" << std::endl;
               std::cout << "Guards must be in the following vertices : " << std::endl;
               print_guards(red , cdt );
          }else if(selected == BLUE){
               std::cout << "Selected color is blue" << std::endl;
               std::cout << "Guards must be in the following vertices : " << std::endl;
               print_guards(blue , cdt );
          }else if(selected == YELLOW){
               std::cout << "Selected color is yellow" << std::endl;
               std::cout << "Guards must be in the following vertices : " << std::endl;
               print_guards(yellow , cdt );
          }
          
          std::cout << "================================================"<< std::endl;
          std::cout << "Each color has the following number of vertices"<< std::endl;
          std::cout << "red " << red.size() << std::endl;
          std::cout << "blue " << blue.size() << std::endl;
          std::cout << "yellow " << yellow.size() << std::endl;
          std::cout << "================================================"<< std::endl;
          
          std::cout << "There are " <<  facelets_count << " facets in the domain." << std::endl;
          
          QApplication app(argc, argv);
          
          QApplication app(argc, argv);

          QWidget window;

          window.resize(250, 150);
          window.setWindowTitle("Simple example");
          window.show();

          return app.exec();
          
//           return 0;
}