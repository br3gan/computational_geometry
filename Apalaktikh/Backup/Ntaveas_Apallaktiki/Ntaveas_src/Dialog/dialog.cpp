#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) : QDialog(parent), ui(new Ui::Dialog){
    ui->setupUi(this);
}

Dialog::~Dialog(){
    delete ui;
}

void Dialog::paintEvent(QPaintEvent*){

    QPainter painter(this);

    //input stream
    std::ifstream ifs ("./results.txt", std::ifstream::in);
    if (ifs < 0)
        std::cout << "Cannot open file" << std::endl;

    std::list<QPoint> guards;
    std::list<QPoint> points;

    char buffer[32] ; int i = 0;
    double vertices[2] ; int v = 0;

    //initialize buffers
    for( i = 0 ; i < 2 ; i++){
       vertices[i] = 0.0;
    }
    for( i = 0 ; i < 32 ; i++){
        buffer[i] = '\0';
    }

    i = 0;

    //Read Guards
    char c = ifs.get();
    while ( ifs.good() && c!= '*'){
        v = 0;
        while( c != '\n' && c!= '*' && c != EOF ){
            i = 0;
            while( c != ' ' &&  c!= '*' && c != '\n' && c != EOF){

                buffer[i++] = c;
                c = ifs.get();
            }
            vertices[v++] = atof(buffer);
            while( i >= 0 )
                buffer[i--]='\0';

            if( c == '\n' || c == '*')
                break;
            c = ifs.get();
        }
        guards.push_back(QPoint( vertices[0] , vertices[1] ) );
        while(v>=0)
            vertices[v--] = 0.0;
        c = ifs.get();
    }

    QPen pointpen(Qt::black);
    pointpen.setWidth(10);
    pointpen.setColor(Qt::black);
    painter.setPen(pointpen);

    for(std::list<QPoint>::iterator it = guards.begin(); it != guards.end(); ++it){
        //std::cout << it->x() << " " << it->y() << std::endl;
        painter.drawPoint(QPoint(it->x() , it->y() ));
    }
    //std::cout << "*" << std::endl;

    //reinitialize buffers
    for( i = 0 ; i < 2 ; i++){
       vertices[i] = 0.0;
    }
    for( i = 0 ; i < 32 ; i++){
        buffer[i] = '\0';
    }

    //Read Points
    c = ifs.get();c = ifs.get();
    while ( ifs.good()) {
        v = 0;
        while( c != '\n' && c != EOF ){
            i = 0;
            while( c != ' ' && c != '\n' && c != EOF){

                buffer[i++] = c;
                c = ifs.get();
            }

            vertices[v++] = atof(buffer);
            while( i >= 0 )
                buffer[i--]='\0';

            if( c == '\n' )
                break;
            c = ifs.get();
        }

        points.push_back(QPoint( vertices[0] , vertices[1] ) );
        while(v>=0)
            vertices[v--] = 0.0;
        c = ifs.get();
    }

    QPen polypen;
    polypen.setWidth(2);
    polypen.setColor(Qt::blue);

    QPolygon poly;

    for(std::list<QPoint>::iterator it = points.begin(); it != points.end(); ++it){
        //std::cout << it->x() << " " << it->y() << std::endl;
        poly.push_back(QPoint(it->x() , it->y() ) );
    }

    painter.setPen(polypen);
    painter.drawPolygon(poly);

    //std::cout << "end of funct" << std::endl;

    //close input file
    ifs.close();

}
