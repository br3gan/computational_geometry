#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QtGui>
#include <QtCore>

#include <iostream>
#include <fstream>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

protected:
    void paintEvent(QPaintEvent *e);

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
