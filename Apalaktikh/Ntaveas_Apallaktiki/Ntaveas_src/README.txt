=============================================
How to compile: |
----------------
Step 1)
  >>./cgalqt_script

Step 2)
  >>cmake .

Step 3)
  >>make

=============================================
How to run:     |
----------------

>>./Museum

=============================================
Odeigeis        |
----------------

Dothentwn tws shmeiwn to programma vriskei to
perivlima pou sxhmatizoun (CTRL+H), kai me thn
epilogh Tools -> Find Guards topothetei tous
filakes 

=============================================
Info            |
----------------

To proramma upostirizei grafikh eisodo kai 
eksodo, alla ta shmeia prepei na dothoun me 
fora sumfona me tous deiktes tou rologiou (CW)

Episis DEN prepei na dothoun dipla shmeia

Den upostirizontai overlapping polugona

Periexetai fakelos "Shapes" pou exei orismena
deigmata polugonwn. Mporoun na dothoun sto
programma  apo to menou: File -> Load points

=============================================
