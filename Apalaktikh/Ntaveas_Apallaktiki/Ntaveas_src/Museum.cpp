#include <fstream>
#include <cmath>

// CGAL headers
#include <CGAL/Cartesian.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/convex_hull_2.h>
#include <CGAL/point_generators_2.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Real_timer.h>

// Qt headers
#include <QtGui>
#include <QString>
#include <QActionGroup>
#include <QFileDialog>
#include <QInputDialog>
#include <QGraphicsRectItem>

// GraphicsView items and event filters (input classes)

#include <CGAL/Qt/PointsGraphicsItem.h>
#include <CGAL/Qt/PolygonGraphicsItem.h>
#include <CGAL/Qt/GraphicsViewPolylineInput.h>

// for viewportsBbox
#include <CGAL/Qt/utility.h>

// the two base classes
#include "ui_Museum.h"   //Ayto prepei na tropopoih8ei katallhla analoga me to onoma toy arxeioy
#include <CGAL/Qt/DemosMainWindow.h>


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::Point_2 Point_2;
typedef K::Iso_rectangle_2 Iso_rectangle_2;

typedef CGAL::Polygon_2<K> Polygon_2;

class MainWindow :
public CGAL::Qt::DemosMainWindow, public Ui::ConvexHull
{
     Q_OBJECT

     private: // Data Memebers
          
          // Scene
          QGraphicsScene scene;  
          
          // Models
          Polygon_2 hull;
          std::vector<Point_2> points;
          std::vector<Point_2> guards;
          
          // Graphics Items
          CGAL::Qt::PointsGraphicsItem<std::vector<Point_2> > * pgi;
          CGAL::Qt::PolygonGraphicsItem<Polygon_2> * hull_gi;
          CGAL::Qt::PointsGraphicsItem<std::vector<Point_2> > * guards_ui;
          
          // Input Filters
          CGAL::Qt::GraphicsViewPolylineInput<K> * pi;
               
     private: // Functions
          
          void computeHull();
          void computeGuards();
          void writeTextLog(const QString&);
          
          void clearPoints();
          void clearHull();
          void clearGuards();
          void clearTextLog();
               
     public:
          
          MainWindow();
          
     public slots:
          
          void processInput(CGAL::Object o);
          
          void on_actionComputeHull_triggered();
          
          void on_actionInsertPoint_toggled(bool checked);
          
          void on_actionInsertRandomPoints_triggered();
          
          void on_actionLoadPoints_triggered();
          
          void on_actionSavePoints_triggered();
          
          void on_actionFindGuards_triggered();
          
          void on_actionClear_triggered();
          
          virtual void open(QString fileName);
     
     signals:
     
          void pointsChanged();
          void guardsChanged();
          void HullChanged();
};
     
     
MainWindow::MainWindow(): DemosMainWindow()
{
     setupUi(this);
     
     // Graphics Item for the input point set
     pgi = new CGAL::Qt::PointsGraphicsItem<std::vector<Point_2> >(&points);
     
     QObject::connect(this, SIGNAL(pointsChanged()), pgi, SLOT(modelChanged()));
     pgi->setVerticesPen(QPen(Qt::black, 4, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
     scene.addItem(pgi);
     
     // Graphics Item for the convex hull
     hull_gi = new CGAL::Qt::PolygonGraphicsItem<Polygon_2>(&hull);
     
     QObject::connect(this, SIGNAL(HullChanged()), hull_gi, SLOT(modelChanged()));
     hull_gi->setEdgesPen(QPen(Qt::black, 0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
     scene.addItem(hull_gi);
     
     // Graphics Item for the guards
     guards_ui = new CGAL::Qt::PointsGraphicsItem<std::vector<Point_2> >(&guards);
     
     QObject::connect(this, SIGNAL(guardsChanged()), guards_ui, SLOT(modelChanged()));
     guards_ui->setVerticesPen(QPen(Qt::blue, 10, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
     scene.addItem(guards_ui);
     
     // Setup input handlers. They get events before the scene gets them
     // and the input they generate is passed to GraphicsViewPolylineInput with 
     // the signal/slot mechanism    
     pi = new CGAL::Qt::GraphicsViewPolylineInput<K>(this, &scene, 1);
     QObject::connect(pi, SIGNAL(generate(CGAL::Object)), this, SLOT(processInput(CGAL::Object)));
     // 
     // Manual handling of actions
     //
     QObject::connect(this->actionQuit, SIGNAL(triggered()), this, SLOT(close()));
     
     //
     // Setup the scene and the view
     //
     scene.setItemIndexMethod(QGraphicsScene::NoIndex);
     scene.setSceneRect(-200, -200, 200, 200);
     // graphicsView inherited from Ui::Bounding_volumes
     this->graphicsView->setScene(&scene);
     this->graphicsView->setMouseTracking(true);
     
     // Turn the vertical axis upside down
     //this->graphicsView->matrix().scale(1, -1);
     this->graphicsView->scale(1, -1);
     
     // The navigation adds zooming and translation functionality to the
     // QGraphicsView:
     // addNavigation() (implemented in DemosMainWindow)
     // adds a CGAL::Qt::GraphicsViewNavigation object as a navigation filter 
     // to graphicsView (inherited member of Ui::Bounding_volumes)
     // All the following functions and signal openRecentFile() are implemented in DemosMainWindow
     this->addNavigation(this->graphicsView);
     
     this->setupStatusBar();
     this->setupOptionsMenu();
     
     this->addRecentFiles(this->menuFile, this->actionQuit);
     connect(this, SIGNAL(openRecentFile(QString)), this, SLOT(open(QString)));
}
     
void
MainWindow::computeHull(){
     // Time measurements
     CGAL::Real_timer t;
     t.start();
     // Compute Hull
     
     for(int i = 0 ; i < points.size() ; i++){
          hull.push_back(points[i]);
     }
     
     t.stop();
     // Prepare Qstring for textLog
     QString str = QString("#points in convex hull: %1\nreal time elapsed: %2 seconds\n").arg(hull.size()).arg(t.time());
     writeTextLog(str);
}

void
MainWindow::writeTextLog(const QString& s){
     // setPlainText discards previous content
     this->textLog->setPlainText(s);
}

void
MainWindow::clearPoints(){
     points.clear();
}

void
MainWindow::clearGuards(){
     guards.clear();
}

void 
MainWindow::clearHull(){
     hull.clear();
}

void
MainWindow::clearTextLog(){
     this->textLog->clear();
}

void
MainWindow::processInput(CGAL::Object o)
{
     //std::cout << "inside processInput.." << std::endl;
     
     std::list<Point_2> input;
     if(CGAL::assign(input, o)){
          Point_2 p = input.front();
          points.push_back(p);
          emit(pointsChanged());
     }
}

/* 
     *  Qt Automatic Connections
     *  http://doc.trolltech.com/4.4/designer-using-a-component.html#automatic-connections
     * 
     *  setupUi(this) generates connections to the slots named
     *  "on_<action_name>_<signal_name>"
     */

void
MainWindow::on_actionInsertPoint_toggled(bool checked)
{
     if(checked){
          scene.installEventFilter(pi);
     } else {
          scene.removeEventFilter(pi);
     }
}

void
MainWindow::on_actionComputeHull_triggered()
{
     clearHull();
     clearGuards();
     computeHull();
     
     emit(HullChanged());
}

void
MainWindow::on_actionClear_triggered()
{
     clearPoints();
     clearHull();
     clearGuards();
     clearTextLog();
     
     emit(pointsChanged());
     emit(HullChanged());
}

void
MainWindow::on_actionInsertRandomPoints_triggered()
{
     QRectF rect = CGAL::Qt::viewportsBbox(&scene);
     CGAL::Qt::Converter<K> convert;  
     Iso_rectangle_2 isor = convert(rect);
     CGAL::Random_points_in_iso_rectangle_2<Point_2> pg((isor.min)(), (isor.max)());
     bool ok = false;
     const int number_of_points = 
     QInputDialog::getInteger(this, 
                              tr("Number of random points"),
                              tr("Enter number of random points"),
                              100,
                              0,
                              (std::numeric_limits<int>::max)(),
                              1,
                              &ok);
     
     if(!ok) {
          return;
     }
     
     // wait cursor
     QApplication::setOverrideCursor(Qt::WaitCursor);
     for(int i = 0; i < number_of_points; ++i){
          Point_2 p = *pg++;
          points.push_back(p);
     }
     
     // default cursor
     QApplication::restoreOverrideCursor();
     emit(pointsChanged());
}


void
MainWindow::on_actionLoadPoints_triggered()
{
     QString fileName = QFileDialog::getOpenFileName(this,
                                                       tr("Open Points file"),
                                                       ".");
     if(! fileName.isEmpty()){
          open(fileName);
     }
}
     
     
void
MainWindow::open(QString fileName)
{
     // wait cursor
     QApplication::setOverrideCursor(Qt::WaitCursor);
     std::ifstream ifs(qPrintable(fileName));
     
     clearPoints();
     clearHull();
     clearGuards();
     clearTextLog();
     
     K::Point_2 p;
     while(ifs >> p) {
          points.push_back(p);
     }
     
     // default cursor
     QApplication::restoreOverrideCursor();
     this->addToRecentFiles(fileName);
     
     emit(pointsChanged());
     emit(guardsChanged());
     emit(HullChanged());
     
}
     
void
MainWindow::on_actionFindGuards_triggered()
{
     computeGuards();
     emit(guardsChanged());
}    
     
void
MainWindow::on_actionSavePoints_triggered()
{
     QString fileName = QFileDialog::getSaveFileName(this,
                                                       tr("Save points"),
                                                       ".");
     if(! fileName.isEmpty()){
          std::ofstream ofs(qPrintable(fileName));
          for(std::vector<Point_2>::const_iterator  
               vit = points.begin(),
               end = points.end();
          vit!= end; ++vit)
          {
               ofs << *vit << std::endl;
          }
     }
}

// void computeGuards();

#include "Museum.moc"  //ayto prepei na tropopoih8ei analoga me to onoma toy arxeioy

int main(int argc, char **argv)
{
     QApplication app(argc, argv);
     
     MainWindow mainWindow;
     mainWindow.show();
     return app.exec();
}


/////////////////////////////////////////////compute guards/////////////////////////////////////////////


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>
#include <CGAL/Polygon_2.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <unistd.h>

//Colors
#define RED 1
#define BLUE 2
#define YELLOW 3

//Edges
#define FIRST_VERTEX 0
#define SECOND_VERTEX 1
#define THIRD_VERTEX 2

//Structs to save Face info
struct Face
{
     Face(){}
     int is_overlapping;
     bool complete;
     
     bool in_hull(){
          return is_overlapping%2 == 1;
     }
     bool is_complete(){
          return complete;
     }
};

//Struct to save Vertex info
struct Vertex
{
     
     Vertex(){}
     //1 - red , 2 - blue , 3 - yellow
     int color;
     //true if edge is colored
     bool is_colored;
     //unique vertex id 
     int vertex_id;
     
     int get_color(){
          return color;
     }
     bool get_colored(){
          return is_colored;
     }
     int get_v_id(){
          return vertex_id;
     }
};

typedef CGAL::Exact_predicates_inexact_constructions_kernel       K;
typedef CGAL::Triangulation_vertex_base_with_info_2<Vertex,K> Vbb;
typedef CGAL::Triangulation_face_base_with_info_2<Face,K>    Fbb;
typedef CGAL::Constrained_triangulation_face_base_2<K,Fbb>        Fb;
typedef CGAL::Triangulation_data_structure_2<Vbb,Fb>              TDS;
typedef CGAL::Exact_predicates_tag                                Itag;
typedef CGAL::Constrained_Delaunay_triangulation_2<K, TDS, Itag>  CDT;
typedef CDT::Point                                                Point;
typedef CGAL::Polygon_2<K>                                        Polygon_2;
typedef CDT::Face_iterator Face_iterator;

//We have to remove extra edges and keep the hull of the non-convex polygon
//**code from CGAL libraly: Triangulation_2/polygon_triangulation.cpp**
void save_hull(CDT& ct, CDT::Face_handle start, int index, std::list<CDT::Edge>& border ){
     
     if(start->info().is_overlapping != -1){
          return;
     }
     std::list<CDT::Face_handle> queue;
     queue.push_back(start);
     
     while(! queue.empty()){
          CDT::Face_handle fh = queue.front();
          queue.pop_front();
          if(fh->info().is_overlapping == -1){
               fh->info().is_overlapping = index;
               for(int i = FIRST_VERTEX; i <= THIRD_VERTEX; i++){
                    CDT::Edge e(fh,i);
                    CDT::Face_handle n = fh->neighbor(i);
                    if(n->info().is_overlapping == -1){
                         if(ct.is_constrained(e)) border.push_back(e);
                         else queue.push_back(n);
                    }
               }
          }
     }
}

void save_hull(CDT& cdt)
{
     for(CDT::All_faces_iterator it = cdt.all_faces_begin(); it != cdt.all_faces_end(); ++it){
          it->info().is_overlapping = -1;
     }
     
     int index = 0;
     std::list<CDT::Edge> border;
     save_hull(cdt, cdt.infinite_face(), index++, border);
     while(! border.empty()){
          CDT::Edge e = border.front();
          border.pop_front();
          CDT::Face_handle n = e.first->neighbor(e.second);
          if(n->info().is_overlapping == -1){
               save_hull(cdt, n, e.first->info().is_overlapping + 1, border);
          }
     }
}

//constrained triangulation of polygon
void insert_polygon(CDT& cdt,const Polygon_2& polygon){
     if ( polygon.is_empty() ) return;
     CDT::Vertex_handle v_prev=cdt.insert(*CGAL::cpp0x::prev(polygon.vertices_end()));
     for (Polygon_2::Vertex_iterator vit=polygon.vertices_begin();
          vit!=polygon.vertices_end();++vit)
          {
               CDT::Vertex_handle vh=cdt.insert(*vit);
               cdt.insert_constraint(vh,v_prev);
               v_prev=vh;
          }
}

//Returns 0 if vertex id is not inside the vector
//Returns 1 if vertex id exists inside the vector
bool element_inside_vector(int v_id , std::vector<int> id_and_color){
     int count = 0;
     for (int i = 0; i <= id_and_color.size(); i++) {
          if( v_id == id_and_color[i]){
               count++;
               break;
          }
     }
     
     if(count > 0){
          return true;
     }else{
          return false;
     }
}

//colorize a yet non visited vertex 
void color_vertex( CDT::Finite_faces_iterator& it , std::vector<int>& color , int edge , int color_id ){
     
     if (element_inside_vector(it->vertex(edge)->info().vertex_id, color) == false) {
          it->vertex(edge)->info().color = color_id;
          color.push_back(it->vertex(edge)->info().vertex_id);
          it->info().complete = true;
          return;
     }
     it->info().complete = true;
     return;
}

//colorize the vertex of an edge
int handle_edges( CDT::Finite_faces_iterator& it , std::vector<int>& one_color , std::vector<int>& other_color , int one_color_id , int other_color_id , int one_vertex , int other_vertex ){
     
     
     
     if (it->vertex(one_vertex)->info().color == one_color_id) {
          std::cout << "subcase 1 , other_color: " << other_color_id << ", one_color:" << one_color_id << std::endl;
          color_vertex( it , other_color , other_vertex , other_color_id );
          return 1;
          
     }
     else if(it->vertex(other_vertex)->info().color == other_color_id){
          std::cout << "subcase 2 , one_color: " << one_color_id << ", other_color:" << other_color_id << std::endl;
          color_vertex( it , one_color , one_vertex , one_color_id );
          return 1;
     }
     else if(it->vertex(one_vertex)->info().color == other_color_id){
          std::cout << "subcase 3 , one_color: " << one_color_id << ", other_color:" << other_color_id << std::endl;
          color_vertex( it , one_color , other_vertex , one_color_id);
          return 1;
          
     }
     else if(it->vertex(other_vertex)->info().color == one_color_id){
          std::cout << "subcase 4 , other_color: " << other_color_id << ", one_color:" << one_color_id << std::endl;
          color_vertex( it , other_color , one_vertex , other_color_id );
          return 1;
          
     }
     return 0;
}


//chooses what color is the least displayed and returns its code
int pick_color( int red , int blue , int yellow){
     
     int min = red;
     
     if (blue < min)
          min = blue;
     
     if (yellow < min )
          min = yellow;
     
     if( min == red )
          return RED;
     if( min == blue )
          return BLUE;
     
     return YELLOW; 
}

//prints the verices of the positioned guards
void print_guards( std::vector<int>& color , CDT& cdt , std::vector<Point_2>& guards ){
     
     for (int i = 0; i < color.size(); i++) {
          bool get_me_out = false;
          for (CDT::Finite_faces_iterator it=cdt.finite_faces_begin();
               it!=cdt.finite_faces_end();++it)
               {
                    if ( it->info().in_hull() ){
                         
                         for (int k=FIRST_VERTEX; k <= THIRD_VERTEX; k++) {
                              
                              if (color.at(i) == it->vertex(k)->info().get_v_id() ) {
                                   
                                   guards.push_back(it->vertex(k)->point());
                                   
                                   std::cout << it->vertex(k)->point() << std::endl;
                                   get_me_out = true;
                              }
                         }
                    }
                    if(get_me_out == true){
                         break;
                    }
               }
     }
}

//initialize the id and infos of the vertices 
int initialization( CDT& cdt , int& facelets_count , int& vertex_id ){
     
     for (CDT::Finite_faces_iterator it=cdt.finite_faces_begin(); it!=cdt.finite_faces_end(); ++it){
          if ( it->info().in_hull() ){
               it->info().complete = false;
               
               for( int edge = FIRST_VERTEX ; edge <= THIRD_VERTEX ; edge++ ){
                    it->vertex(edge)->info().is_colored = false;
                    it->vertex(edge)->info().vertex_id = vertex_id++;
               }
               facelets_count++;
          }
     }
     
     return facelets_count;
}

//read the points from file, returns tha polygon
Polygon_2 read_points( char* filename){
     
     //input stream
     std::ifstream ifs (filename, std::ifstream::in);
     
     char buffer[32] ; int i = 0;
     double vertices[2] ; int v = 0;
     
     //initialize buffers
     for( i = 0 ; i < 2 ; i++){
          vertices[i] = 0.0;
     }
     for( i = 0 ; i < 32 ; i++){
          buffer[i] = '\0';
     }
     
     i = 0;
     
     Polygon_2 polygon1;
     
     char c = ifs.get();
     
     //Read Points
     while (ifs.good()) {
          v = 0; 
          while( c != '\n' && c != EOF ){
               i = 0;
               while( c != ' ' && c != '\n' && c != EOF){
                    
                    buffer[i++] = c;
                    c = ifs.get();
               }

               vertices[v++] = atof(buffer);
               while( i >= 0 )
                    buffer[i--]='\0';
               
               if( c == '\n' )
                    break;
               c = ifs.get();
          }
          polygon1.push_back(Point(vertices[0] * 0.5 , vertices[1] * 0.5 ));
          while(v>=0)
               vertices[v--] = 0.0;
          c = ifs.get();
     }
     
     //close input file
     ifs.close();
     return polygon1;
}

//print the color from its code
std::string print_color( int color ){
     
     if (color == 1)
          return "red";
     if (color == 2)
          return "blue";
     if (color == 3)
          return "yellow"; 
}

void MainWindow::computeGuards(){
          
     Polygon_2 polygon1;
     
     polygon1 = hull;

     CDT cdt;
     insert_polygon(cdt,polygon1);
     save_hull(cdt);

     int vertex_id = 1;
     int facelets_count = 0;
     
     //initialize vertex info, find how many facelets we created
     initialization( cdt , facelets_count , vertex_id );
     
     int count=0;
     
     //create vectors for each color 
     std::vector<int> red;
     std::vector<int> blue;
     std::vector<int> yellow;
     for (int facelets = 0; facelets<=facelets_count; facelets++) {
          
          //3-Color edges
          for (CDT::Finite_faces_iterator it=cdt.finite_faces_begin(); it!=cdt.finite_faces_end();++it )
               {
                    if ( it->info().in_hull() ){
                         
                         //if the triangle is not visited, colorize all the edges
                         if(count == 0){ 
                              it->vertex(FIRST_VERTEX)->info().color = RED;
                              red.push_back(it->vertex(FIRST_VERTEX)->info().vertex_id);
                              it->vertex(SECOND_VERTEX)->info().color = BLUE;
                              blue.push_back(it->vertex(SECOND_VERTEX)->info().vertex_id);
                              it->vertex(THIRD_VERTEX)->info().color = YELLOW;
                              yellow.push_back(it->vertex(THIRD_VERTEX)->info().vertex_id);
                              
                              for(int vert = FIRST_VERTEX ; vert <= THIRD_VERTEX ; vert++ )
                                   it->vertex(vert)->info().is_colored = true;
                              
                              it->info().complete = true;
                              
                         }
                         //if the triangle has at least one or two uncolored verices
                         //color it/them according to the following rules 
                         else{
                              if(it->info().complete == false  ){
                                   
                                   //case first vertex = blue
                                   if (element_inside_vector(it->vertex(FIRST_VERTEX)->info().vertex_id, blue) == true) {
                                        std::cout << "case 1 " << std::endl;
                                        if (handle_edges( it , red , yellow , RED , YELLOW , SECOND_VERTEX , THIRD_VERTEX ) == 1 ){
                                             std::cout << "triangle " << count + 1 << "/" << facelets_count << ":" << std::endl;
                                             std::cout << "(" << it->vertex(FIRST_VERTEX)->point() << ") | color: " << print_color( it->vertex(FIRST_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(SECOND_VERTEX)->point() << ") | color: " << print_color( it->vertex(SECOND_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(THIRD_VERTEX)->point() << ") | color: " << print_color( it->vertex(THIRD_VERTEX)->info().get_color() ) << std::endl;
                                             continue;}
                                   }
                                   //case first vertex = red
                                   else if (element_inside_vector(it->vertex(FIRST_VERTEX)->info().vertex_id, red) == true) {
                                        std::cout << "case 2 " << std::endl;
                                        if (handle_edges( it , blue , yellow , BLUE , YELLOW , SECOND_VERTEX , THIRD_VERTEX ) == 1 ){
                                             std::cout << "triangle " << count + 1 << "/" << facelets_count << ":" << std::endl;
                                             std::cout << "(" << it->vertex(FIRST_VERTEX)->point() << ") | color: " << print_color( it->vertex(FIRST_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(SECOND_VERTEX)->point() << ") | color: " << print_color( it->vertex(SECOND_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(THIRD_VERTEX)->point() << ") | color: " << print_color( it->vertex(THIRD_VERTEX)->info().get_color() ) << std::endl;
                                             continue;}
                                   }
                                   //case first vertex = yellow
                                   else if (element_inside_vector(it->vertex(FIRST_VERTEX)->info().vertex_id, yellow) == true) {
                                        std::cout << "case 3 " << std::endl;
                                        if (handle_edges( it , red , blue , RED , BLUE , SECOND_VERTEX , THIRD_VERTEX ) == 1 ){
                                             std::cout << "triangle " << count + 1 << "/" << facelets_count << ":" << std::endl;
                                             std::cout << "(" << it->vertex(FIRST_VERTEX)->point() << ") | color: " << print_color( it->vertex(FIRST_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(SECOND_VERTEX)->point() << ") | color: " << print_color( it->vertex(SECOND_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(THIRD_VERTEX)->point() << ") | color: " << print_color( it->vertex(THIRD_VERTEX)->info().get_color() ) << std::endl;
                                             continue;}
                                   }
                                   //case second vertex = blue
                                   if(element_inside_vector(it->vertex(SECOND_VERTEX)->info().vertex_id, blue) == true){
                                        std::cout << "case 4 " << std::endl;
                                        if (handle_edges( it , red , yellow , RED , YELLOW , FIRST_VERTEX , THIRD_VERTEX ) == 1 ){
                                             std::cout << "triangle " << count + 1 << "/" << facelets_count << ":" << std::endl;
                                             std::cout << "(" << it->vertex(FIRST_VERTEX)->point() << ") | color: " << print_color( it->vertex(FIRST_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(SECOND_VERTEX)->point() << ") | color: " << print_color( it->vertex(SECOND_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(THIRD_VERTEX)->point() << ") | color: " << print_color( it->vertex(THIRD_VERTEX)->info().get_color() ) << std::endl;
                                             continue;}
                                   //case second vertex = red    
                                   }else if(element_inside_vector(it->vertex(SECOND_VERTEX)->info().vertex_id, red) == true){
                                        std::cout << "case 5 " << std::endl;
                                        if (handle_edges( it , blue , yellow , BLUE , YELLOW , FIRST_VERTEX , THIRD_VERTEX ) == 1 ){
                                             std::cout << "triangle " << count + 1 << "/" << facelets_count << ":" << std::endl;
                                             std::cout << "(" << it->vertex(FIRST_VERTEX)->point() << ") | color: " << print_color( it->vertex(FIRST_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(SECOND_VERTEX)->point() << ") | color: " << print_color( it->vertex(SECOND_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(THIRD_VERTEX)->point() << ") | color: " << print_color( it->vertex(THIRD_VERTEX)->info().get_color() ) << std::endl;
                                             continue;}
                                        
                                   }
                                   //case second vertex = yellow
                                   else if(element_inside_vector(it->vertex(SECOND_VERTEX)->info().vertex_id, yellow) == true){
                                        std::cout << "case 6 " << std::endl;
                                        if (handle_edges( it , red , blue , RED , BLUE , FIRST_VERTEX , THIRD_VERTEX ) == 1 ){
                                             std::cout << "triangle " << count + 1 << "/" << facelets_count << ":" << std::endl;
                                             std::cout << "(" << it->vertex(FIRST_VERTEX)->point() << ") | color: " << print_color( it->vertex(FIRST_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(SECOND_VERTEX)->point() << ") | color: " << print_color( it->vertex(SECOND_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(THIRD_VERTEX)->point() << ") | color: " << print_color( it->vertex(THIRD_VERTEX)->info().get_color() ) << std::endl;
                                             continue;}
                                        
                                   }
                                   //case third vertex = blue
                                   if(element_inside_vector(it->vertex(THIRD_VERTEX)->info().vertex_id, blue) == true){
                                        std::cout << "case 7 " << std::endl;
                                        if (handle_edges( it , red , yellow , RED , YELLOW , SECOND_VERTEX , FIRST_VERTEX ) == 1 ){
                                             std::cout << "triangle " << count + 1 << "/" << facelets_count << ":" << std::endl;
                                             std::cout << "(" << it->vertex(FIRST_VERTEX)->point() << ") | color: " << print_color( it->vertex(FIRST_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(SECOND_VERTEX)->point() << ") | color: " << print_color( it->vertex(SECOND_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(THIRD_VERTEX)->point() << ") | color: " << print_color( it->vertex(THIRD_VERTEX)->info().get_color() ) << std::endl;
                                             continue;}
                                   }
                                   //case third vertex = red
                                   else if(element_inside_vector(it->vertex(THIRD_VERTEX)->info().vertex_id, red) == true){
                                        std::cout << "case 8 " << std::endl;
                                        if (handle_edges( it , blue , yellow , BLUE , YELLOW , SECOND_VERTEX , FIRST_VERTEX ) == 1 ){
                                             std::cout << "triangle " << count + 1 << "/" << facelets_count << ":" << std::endl;
                                             std::cout << "(" << it->vertex(FIRST_VERTEX)->point() << ") | color: " << print_color( it->vertex(FIRST_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(SECOND_VERTEX)->point() << ") | color: " << print_color( it->vertex(SECOND_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(THIRD_VERTEX)->point() << ") | color: " << print_color( it->vertex(THIRD_VERTEX)->info().get_color() ) << std::endl;
                                             continue;}
                                        
                                   }
                                   //case third vertex = yellow
                                   else if(element_inside_vector(it->vertex(THIRD_VERTEX)->info().vertex_id, yellow) == true){
                                        std::cout << "case 9 " << std::endl;
                                        if (handle_edges( it , red , blue , RED , BLUE , SECOND_VERTEX , FIRST_VERTEX ) == 1 ){
                                             std::cout << "triangle " << count + 1 << "/" << facelets_count << ":" << std::endl;
                                             std::cout << "(" << it->vertex(FIRST_VERTEX)->point() << ") | color: " << print_color( it->vertex(FIRST_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(SECOND_VERTEX)->point() << ") | color: " << print_color( it->vertex(SECOND_VERTEX)->info().get_color() ) << std::endl;
                                             std::cout << "(" << it->vertex(THIRD_VERTEX)->point() << ") | color: " << print_color( it->vertex(THIRD_VERTEX)->info().get_color() ) << std::endl;
                                             continue;}
                                        
                                   }
                              }
                         }
                         count++;
                    }
               }
     }
     
     int triangle = 0;
     
     for (CDT::Finite_faces_iterator it = cdt.finite_faces_begin(); it!=cdt.finite_faces_end() ; ++it){
          if ( it->info().in_hull() ){
               std::cout << "triangle " << triangle + 1 << "/" << facelets_count << ":" << std::endl;
               std::cout << "(" << it->vertex(FIRST_VERTEX)->point() << ") | color: " << print_color( it->vertex(FIRST_VERTEX)->info().get_color() ) << std::endl;
               std::cout << "(" << it->vertex(SECOND_VERTEX)->point() << ") | color: " << print_color( it->vertex(SECOND_VERTEX)->info().get_color() ) << std::endl;
               std::cout << "(" << it->vertex(THIRD_VERTEX)->point() << ") | color: " << print_color( it->vertex(THIRD_VERTEX)->info().get_color() ) << std::endl;
               std::cout <<"......................................................................" << std::endl;
               
               triangle++;
          }
     }
     
     //find the least displayed color
     int selected = pick_color( red.size() , blue.size() , yellow.size() );
     
     std::cout << "Selected color is " << print_color(selected) << std::endl;
     
     if(selected == RED){
          std::cout << "Guards positions: " << std::endl;
          print_guards(red , cdt , guards);
     }else if(selected == BLUE){
          std::cout << "Guards positions: " << std::endl;
          print_guards(blue , cdt , guards);
     }else if(selected == YELLOW){
          std::cout << "Guards positions:" << std::endl;
          print_guards(yellow , cdt , guards);
     }
     
     std::cout << "================================================"<< std::endl;
     std::cout << "Triangulation created:"<< std::endl;
     std::cout << red.size() << " red vertices" << std::endl;
     std::cout << blue.size() << " blue vertices" <<  std::endl;
     std::cout <<  yellow.size() << " yellow vetices" << std::endl << std::endl;;
     std::cout << "and " <<  facelets_count << " facets." << std::endl;
     
}